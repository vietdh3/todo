//
//  ItemManager.swift
//  ToDo
//
//  Created by VietDH3 on 11/13/18.
//  Copyright © 2018 VietDH3. All rights reserved.
//

import UIKit

class ItemManager {
    var toDoCount: Int {return toDoItems.count}
    var doneCount: Int {return doneItems.count}
    private var toDoItems: [ToDoItem] = []
    private var doneItems: [ToDoItem] = []
    func item(at index: Int) -> ToDoItem {
        return toDoItems[index]
    }
    func checkItem(at index: Int) {
        let item = toDoItems.remove(at: index)
        doneItems.append(item)
    }
    func doneItem(at index: Int) -> ToDoItem{
        return doneItems[index]
    }
    func removeAll() {
        toDoItems.removeAll()
        doneItems.removeAll()
    }
    func add( _ item: ToDoItem) {
        if !toDoItems.contains(item) {
            toDoItems.append(item)
        }
    }
}
